const chalk = require('chalk')

function init() {
    console.log('\n' + chalk.blue('┗ 安装完毕 ----------------------------') + '\n')
    console.log(chalk.bold.red(' 请继续使用HBuilderx运行服务') + '\n')
    console.log(chalk.yellow(' --------------- 鸣谢 ----------------'))
    console.log(chalk.yellow('|                                     |'))
    console.log(chalk.yellow('| By: 落魄实习生（Mouxan）            |'))
    console.log(chalk.yellow('| 如有其他问题请联系:                 |'))
    console.log(chalk.yellow('|            QQ: 455171924            |'))
    console.log(chalk.yellow('|            邮箱: mouxan@163.com     |'))
    console.log(chalk.yellow('|                                     |'))
    console.log(chalk.yellow(' -------------------------------------') + '\n')
}

init()